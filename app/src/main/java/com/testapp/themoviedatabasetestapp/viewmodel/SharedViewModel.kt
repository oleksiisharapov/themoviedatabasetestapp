package com.testapp.themoviedatabasetestapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.repository.MovieRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class SharedViewModel(private val repository: MovieRepository) : ViewModel() {

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        exceptionLiveData.value = exception
    }

    private val observableMovie: MutableLiveData<Movie> = MutableLiveData()
    private val trendingMoviesLiveData = MutableLiveData<List<Movie>>()
    private val searchResultLiveData = MutableLiveData<List<Movie>>()
    private val exceptionLiveData = MutableLiveData<Throwable>()

    val trendingMovies: LiveData<List<Movie>> get() = trendingMoviesLiveData
    val searchResult: LiveData<List<Movie>> get() = searchResultLiveData
    val exception: LiveData<Throwable?> get() = exceptionLiveData

    init {
        fetchTrendingMovies()
    }

    fun getAction() = observableMovie

    fun handleAction(movieId: Int) {
        viewModelScope.launch(coroutineExceptionHandler) {
            observableMovie.value = repository.getMovie(movieId)
        }
    }

    fun search(queryText: String) {
        viewModelScope.launch(coroutineExceptionHandler) {
            searchResultLiveData.value =
                repository.searchMovies(queryText)
        }
    }

    fun clearException(){
        exceptionLiveData.value = null
    }

    fun fetchTrendingMovies() {
        viewModelScope.launch(coroutineExceptionHandler) {
            trendingMoviesLiveData.value = repository.getTrendingMovies()
        }
    }

}