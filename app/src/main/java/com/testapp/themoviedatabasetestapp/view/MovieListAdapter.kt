package com.testapp.themoviedatabasetestapp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testapp.themoviedatabasetestapp.R
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.repository.http.ImageLoader
import com.testapp.themoviedatabasetestapp.repository.http.setImage
import kotlinx.android.synthetic.main.movie_list_item.view.*
import java.net.URL

class MovieListAdapter(var itemList: MutableList<Movie>, val imageLoader: ImageLoader, val clickListener: (Int) -> Unit): RecyclerView.Adapter<MovieListAdapter.MovieVH>(){

    inner class MovieVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindViewHolder(movie: Movie){
            itemView.apply {
                movieTitile.text = movie.movieTitile
                movieDescription.text = movie.movieDescription
                movieReleaseDate.text = movie.releaseDate
                listItemImageView.setImage( URL("http://image.tmdb.org/t/p/w185${movie.imageUrl}"), imageLoader)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieVH {
        val viewHolder = MovieVH(LayoutInflater.from(parent.context).inflate(R.layout.movie_list_item, parent, false))
        viewHolder.itemView.setOnClickListener {
            clickListener(itemList[viewHolder.adapterPosition].id)
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: MovieVH, position: Int) {
        holder.bindViewHolder(itemList[position])
    }

}