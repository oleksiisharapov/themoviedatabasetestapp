package com.testapp.themoviedatabasetestapp.view

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.chip.Chip
import com.testapp.themoviedatabasetestapp.R
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.repository.http.ImageLoader
import com.testapp.themoviedatabasetestapp.repository.http.setImage
import kotlinx.android.synthetic.main.detail_screen_fragment_layout.*
import java.net.URL

class DetailScreenFragment : Fragment() {


    private  var movie: Movie? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_screen_fragment_layout, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            movie = arguments?.getParcelable("movie")
    }

    override fun onResume() {
        super.onResume()
        movie?.let{
            movieNameTextView.text = it.movieTitile
            releaseDateTextView.text = it.releaseDate
            directorTextView.text = it.director.name
            movieGenresTextView.text = "Genres ${it.genres.map { genre -> genre.name }.toString()}"
            it.cast.take(3).forEach {actor ->
                castChipGroup.addView(Chip(context).apply {
                    text = actor.name
                })
            }
            movieDescriptionTextView.text = it.movieDescription
            posterImageView.setImage(URL("http://image.tmdb.org/t/p/w500/${it.imageUrl}"), (activity as ProvidesImageLoader).getImageLoader())
        }
    }
}