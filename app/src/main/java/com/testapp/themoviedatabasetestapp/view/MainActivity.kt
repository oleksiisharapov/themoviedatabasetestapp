package com.testapp.themoviedatabasetestapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import com.testapp.themoviedatabasetestapp.R
import com.testapp.themoviedatabasetestapp.repository.cache.LRUCache
import com.testapp.themoviedatabasetestapp.repository.http.ApiClient
import com.testapp.themoviedatabasetestapp.repository.http.ApiInterface
import com.testapp.themoviedatabasetestapp.repository.http.ImageLoader
import com.testapp.themoviedatabasetestapp.repository.http.MovieRepositoryImplementation
import com.testapp.themoviedatabasetestapp.viewmodel.SharedViewModel


class MainActivity : AppCompatActivity(), ProvidesImageLoader {


    private val viewModelFactory: ViewModelProvider.Factory by lazy {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return SharedViewModel(MovieRepositoryImplementation(ApiClient.createService(ApiInterface::class.java))) as T
            }

        }
    }
    private val sharedViewModel: SharedViewModel  by lazy {
        ViewModelProvider(this, viewModelFactory).get(
            SharedViewModel::class.java
        )
    }



    private val memCache = LRUCache()
    private val  imageLoader = ImageLoader(memCache)

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout)

        navController = findNavController(R.id.navHostFragment)

        sharedViewModel.getAction().observe(this, Observer {
            it?.let {
                val bundle = Bundle()
                bundle.putParcelable("movie", it)
                navController.navigate(R.id.detailScreenFragment, bundle)
            }
        })
        sharedViewModel.exception.observe(this, Observer{
            it?.let{
                showErrorSnackBar("Check internet connection", "Retry"){
                    sharedViewModel.clearException()
                    sharedViewModel.fetchTrendingMovies()
                }
            }
        })
    }

    override fun getImageLoader(): ImageLoader {
        return imageLoader
    }

    private fun showErrorSnackBar(errorMessage: String, actionName: String, callback: ()-> Unit){
        Snackbar.make(window.decorView.rootView, errorMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(actionName){
                callback.invoke()
            }
            .show()
    }

}

interface ProvidesImageLoader{
    fun getImageLoader(): ImageLoader

}
