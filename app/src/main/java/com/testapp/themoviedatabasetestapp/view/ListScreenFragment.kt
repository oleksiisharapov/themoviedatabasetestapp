package com.testapp.themoviedatabasetestapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.testapp.themoviedatabasetestapp.R
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.list_screen_fragment_layout.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ListScreenFragment : Fragment(), SearchView.OnQueryTextListener {


    lateinit var sharedViewModel: SharedViewModel

    private var searchJob: Job? = null

    private var searchItemsCount: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
        return inflater.inflate(R.layout.list_screen_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movieListRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        movieListRecyclerView.adapter =
            MovieListAdapter(arrayListOf(), (activity as ProvidesImageLoader).getImageLoader()) {
                sharedViewModel.handleAction(it)
            }

        sharedViewModel.trendingMovies.observe(this.viewLifecycleOwner,
            Observer<List<Movie>> {
                (movieListRecyclerView.adapter as MovieListAdapter).apply {
                    itemList = it as MutableList<Movie>
                    notifyDataSetChanged()
                }
            })

        sharedViewModel.searchResult.observe(this.viewLifecycleOwner,
            Observer<List<Movie>> {
                (movieListRecyclerView.adapter as MovieListAdapter).apply {
                    if (searchItemsCount > 0 && itemList.size > searchItemsCount) {
                        notifyItemRangeRemoved(0, searchItemsCount)
                        itemList.subList(0, searchItemsCount).clear()
                    }
                    itemList.addAll(0, it)
                    notifyDataSetChanged()
                    searchItemsCount = it.size
                }
            }
        )

        searchView.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        searchJob?.cancel()
        searchJob = this.lifecycleScope.launch {
            newText?.let {
                if (it.isNotEmpty()) {
                    delay(1500)
                    sharedViewModel.search(it)
                }
            }
        }
        return false
    }

}
