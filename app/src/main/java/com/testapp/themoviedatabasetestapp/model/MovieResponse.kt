package com.testapp.themoviedatabasetestapp.model

data class MovieResponse(
    val results: List<Movie>
)