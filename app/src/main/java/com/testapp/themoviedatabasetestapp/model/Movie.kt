package com.testapp.themoviedatabasetestapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Movie(
    val id: Int,

    @SerializedName("original_title")
    val movieTitile: String,

    @SerializedName("overview")
    val movieDescription: String,

    @SerializedName("release_date")
    val releaseDate: String,

    var genres: List<Genre>,
    var director: Actor,
    var cast: List<Actor>,

    @SerializedName("poster_path")
    val imageUrl: String
    ) : Parcelable