package com.testapp.themoviedatabasetestapp.model

data class CreditsResponse(
    val cast: List<Actor>,
    val crew: List<Actor>
)