package com.testapp.themoviedatabasetestapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Actor(
    val name: String
): Parcelable
