package com.testapp.themoviedatabasetestapp.repository.cache

import android.graphics.Bitmap

interface ImageCache{
    fun saveImage(key: String, bitmap: Bitmap)
    fun getImage(key: String): Bitmap?
}