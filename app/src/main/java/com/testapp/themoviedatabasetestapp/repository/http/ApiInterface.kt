package com.testapp.themoviedatabasetestapp.repository.http

import com.testapp.themoviedatabasetestapp.model.CreditsResponse
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface{

    @GET("trending/movie/day")
    suspend fun getTrendingMovies(): MovieResponse

    @GET("movie/{movie_id}")
    suspend fun getMovie(@Path("movie_id") movie_io: Int): Movie

    @GET("movie/{movie_id}/credits")
    suspend fun getDetails(@Path("movie_id") movie_io: Int): CreditsResponse

    @GET("search/movie")
    suspend fun searchMovies(@Query("query") queryText: String): MovieResponse

}