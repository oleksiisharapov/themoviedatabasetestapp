package com.testapp.themoviedatabasetestapp.repository.http

import com.google.gson.GsonBuilder
import com.testapp.themoviedatabasetestapp.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient{

    val baseUrl = "https://api.themoviedb.org/3/"

    private val retrofit: Retrofit by lazy {
        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(OkHttpClient.Builder()
                .addInterceptor {
                    val requestBuilder = it.request().newBuilder()
                    val newUrl = it.request().url().newBuilder().addQueryParameter("api_key", BuildConfig.API_KEY).build()
                    requestBuilder.url(newUrl)
                    it.proceed(requestBuilder.build())
                }
                .build())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
             builder.build()
    }

    fun <T> createService(apiClass: Class<T>) = retrofit.create(apiClass)



}