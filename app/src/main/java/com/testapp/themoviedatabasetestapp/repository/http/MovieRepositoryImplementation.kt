package com.testapp.themoviedatabasetestapp.repository.http

import com.testapp.themoviedatabasetestapp.model.Actor
import com.testapp.themoviedatabasetestapp.model.Movie
import com.testapp.themoviedatabasetestapp.repository.MovieRepository

class MovieRepositoryImplementation(private val apiInterface: ApiInterface) : MovieRepository {


    override suspend fun getTrendingMovies(): List<Movie> {
        return apiInterface.getTrendingMovies().results
    }

    override suspend fun getMovie(movieId: Int): Movie {
        val movie = apiInterface.getMovie(movieId)
        val creditsResponse = apiInterface.getDetails(movieId)
        movie.cast = creditsResponse.cast
        movie.director = creditsResponse.crew.getOrNull(0) ?: Actor("Unknown")
        return movie
    }

    override suspend fun searchMovies(query: String): List<Movie> {
        return apiInterface.searchMovies(query).results
    }


}