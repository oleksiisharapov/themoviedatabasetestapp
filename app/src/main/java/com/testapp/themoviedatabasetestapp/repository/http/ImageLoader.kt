package com.testapp.themoviedatabasetestapp.repository.http

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.ImageView
import com.testapp.themoviedatabasetestapp.repository.cache.ImageCache
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.CoroutineContext

class ImageLoader(private val imageCache: ImageCache) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = IO


    private val loadQuery: MutableMap<Int, Job> = mutableMapOf()

    fun loadImageFromUrl(url: URL, viewId: Int, callback: (Bitmap) -> Unit) {
        loadQuery[viewId] = launch(coroutineContext) {
            try {

                val cachedBitmap = imageCache.getImage(url.toString())

                if(cachedBitmap != null){
                    withContext(Main){
                        callback(cachedBitmap)
                    }
                    return@launch
                }

                val httpConnection = url.openConnection() as HttpURLConnection
                httpConnection.doInput = true
                httpConnection.connect()

                val inputStream = httpConnection.inputStream
                val bitmap = BitmapFactory.decodeStream(inputStream)
                imageCache.saveImage(url.toString(), bitmap)

                withContext(Main) {
                    callback(bitmap)
                }

            } catch (e: Exception) {
                Log.d("Exception", e.toString())
            }
            loadQuery.remove(viewId)
        }
    }

    fun cancelLoading(viewId: Int){
        loadQuery[viewId]?.apply {
            cancel()
            loadQuery.remove(viewId)
        }
    }
}

fun ImageView.setImage(url: URL, imageLoader: ImageLoader) {
    val viewId = this.id
    imageLoader.loadImageFromUrl(url, viewId) {
        setImageBitmap(it)
    }
}



