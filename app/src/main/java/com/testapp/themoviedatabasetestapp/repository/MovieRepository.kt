package com.testapp.themoviedatabasetestapp.repository

import com.testapp.themoviedatabasetestapp.model.Movie

interface MovieRepository {

    suspend fun getTrendingMovies(): List<Movie>
    suspend fun searchMovies(query: String): List<Movie>
    suspend fun getMovie(movieId: Int): Movie

}