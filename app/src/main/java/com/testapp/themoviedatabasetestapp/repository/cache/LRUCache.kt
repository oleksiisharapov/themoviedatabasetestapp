package com.testapp.themoviedatabasetestapp.repository.cache

import android.graphics.Bitmap
import androidx.collection.LruCache

class LRUCache(size: Int = defaultLruCacheSize): ImageCache{
    companion object{
        val defaultLruCacheSize: Int
            get() = ((Runtime.getRuntime().maxMemory() / 1024) / 10).toInt()
    }

    private val cache = LruCache<String, Bitmap>(size)

    override fun saveImage(key: String, bitmap: Bitmap) {
        cache.put(key, bitmap)
    }

    override fun getImage(key: String): Bitmap? {
        return cache[key]
    }

}